#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
sudo apt update -y
sudo apt upgrade -y
curl -s https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner
sudo cat > /etc/apt/preferences.d/pin-gitlab-runner.pref <<EOF
Explanation: Prefer GitLab provided packages over the Debian native ones
Package: gitlab-runner
Pin: origin packages.gitlab.com
Pin-Priority: 1001
EOF
sudo apt install rand gitlab-runner  -y

#
# comment added - let's check it. comment
